import sys, os
os.environ['SPARK_HOME'] = '/usr/local/spark'
sys.path[:0] = ['/usr/local/spark/python', '/usr/local/spark/python/lib/py4j-0.10.9.3-src.zip']
import pyspark
